# gitlab-pages-with-mkdocs

In this project the markdown files in the [docs](docs) directory
are converted into statichtml files and finally puplished to
<https://juergen.kosel.jk.gitlab.io/gitlab-pages-with-mkdocs/index.html>.
Unfortunally the `.` in my user name lead to a certificate warning.

See also <https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html>
